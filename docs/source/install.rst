Installation
=====================

Use git and pip to clone this repository and to install the library, respectively.

::

 $ git clone https://gitlab.com/wxwilcke/pyRDF.git
 $ cd pyRDF/
 $ pip install .

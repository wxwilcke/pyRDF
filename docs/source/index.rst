.. GNN-devtools documentation master file, created by
   sphinx-quickstart on Mon May  1 11:21:45 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Lightweight RDF Stream Parser for Python
========================================

A lightweight RDF and RDF-Star parser which streams triples directly from/to
disk or standard input/output without loading the entire graph into memory.

Supports the N-Triples and N-Quads serialization format.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   modules
   examples


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

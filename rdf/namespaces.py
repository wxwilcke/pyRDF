#!/usr/bin/env python

from rdf.terms import IRIRef


OWL = IRIRef("http://www.w3.org/2002/07/owl#")
RDF = IRIRef("http://www.w3.org/1999/02/22-rdf-syntax-ns#")
RDFS = IRIRef("http://www.w3.org/2000/01/rdf-schema#")
SKOS = IRIRef("http://www.w3.org/2004/02/skos/core#")
XSD = IRIRef("http://www.w3.org/2001/XMLSchema#")

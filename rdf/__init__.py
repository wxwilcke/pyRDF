#!/usr/bin/env python

from rdf.formats import NTriples, NQuads 
from rdf.graph import Statement
from rdf.terms import BNode, IRIRef, Literal
from rdf.namespaces import OWL, RDF, RDFS, SKOS, XSD
